import django
from django.db import models
#from app import models

class SB_Owner(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.EmailField()
    password = models.CharField(max_length=30)
    ownerid = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)

class SB_Game(models.Model):
    home_team = models.CharField(max_length=30)
    away_team = models.CharField(max_length=30)
    gamenum = models.IntegerField()
    gamenumstr = models.CharField(max_length=30)
    game_timestamp = models.DateTimeField(null=True, blank=True)
    startrequest_timestamp = models.DateTimeField(auto_now_add=True)
    in_progress = models.BooleanField(default=False)
    game_length = models.IntegerField(default=1)
    camera_on = models.BooleanField(default=False)
    camera_cnt = models.IntegerField(null=True, blank=True)
    CAM1_id = models.IntegerField(null=True, blank=True)
    CAM1_Start_Time = models.DateTimeField(null=True, blank=True)
    CAM2_id = models.IntegerField(null=True, blank=True)
    CAM2_Start_Time = models.DateTimeField(null=True, blank=True)
    CAM3_id = models.IntegerField(null=True, blank=True)
    CAM3_Start_Time = models.DateTimeField(null=True, blank=True)
    CAM4_id = models.IntegerField(null=True, blank=True)
    CAM4_Start_Time = models.DateTimeField(null=True, blank=True)
    CAM5_id = models.IntegerField(null=True, blank=True)
    CAM5_Start_Time = models.DateTimeField(null=True, blank=True)
    CAM6_id = models.IntegerField(null=True, blank=True)
    CAM6_Start_Time = models.DateTimeField(null=True, blank=True)
    CAM7_id = models.IntegerField(null=True, blank=True)
    CAM7_Start_Time = models.DateTimeField(null=True, blank=True)
    CAM8_id = models.IntegerField(null=True, blank=True)
    CAM8_Start_Time = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return '%s %s %s' (self.home_team, self.away_team, self.gamenum, self.gamenumstr, self.game_timestamp, self.in_progress, self.id, self.game_length, self.startrequest_timestamp)

class SB_UserTag(models.Model):
    tagid = models.IntegerField(null=True, blank=True)
    gamenum = models.IntegerField(null=True, blank=True)
    ownerid = models.IntegerField(null=True, blank=True)
    tagrequest_ts = models.DateTimeField(auto_now_add=True)
    recordtime_ts = models.IntegerField(null=True, blank=True)
    recordtime_min = models.IntegerField(null=True, blank=True)
    recordtime_sec = models.IntegerField(null=True, blank=True)
    clip_process = models.NullBooleanField(default = False)
    clip_link1 = models.CharField(max_length=256, null=True, blank=True)
    clip_rating = models.IntegerField(null=True, blank=True)
    clip_share = models.NullBooleanField(default = False)
    clip_notes = models.CharField(max_length=256, null=True, blank=True)

    def __str__(self):
        return '%s' (self.recordtime_ts, self.clip_notes, self.clip_rating, self.clip_share, self.tagid)

class SB_GameData(models.Model):
    gamenum = models.ForeignKey(SB_Game)
    userid = models.IntegerField()
    file_num = models.IntegerField()
    file_start = models.IntegerField()
    
    def __str__(self):
        return self.name

class SB_RaspberryPiData(models.Model):
    SBunit_id = models.IntegerField()
    camera_on = models.NullBooleanField(default = False)
    idle = models.IntegerField()
    unit_role = models.CharField(max_length=30)
    charfield_a = models.CharField(max_length=30)
    char_field_b = models.CharField(max_length=30)
    counter_a = models.IntegerField()
    counter_b = models.IntegerField()

    def __str__(self):
        return self.name
