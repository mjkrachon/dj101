# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-05-18 22:31
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('DJAPP_201', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='sb_usertag',
            old_name='owner',
            new_name='ownerid',
        ),
        migrations.AddField(
            model_name='sb_owner',
            name='ownerid',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='sb_usertag',
            name='recordtime_ts',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='sb_usertag',
            name='tagrequest_ts',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='sb_game',
            name='game_timestamp',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='sb_game',
            name='startrequest_timestamp',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
