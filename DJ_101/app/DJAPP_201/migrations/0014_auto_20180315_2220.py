# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2018-03-16 02:20
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('DJAPP_201', '0013_auto_20180315_2219'),
    ]

    operations = [
        migrations.AddField(
            model_name='sb_game',
            name='CAM1_Start_Time',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='sb_game',
            name='CAM1_id',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='sb_game',
            name='CAM2_Start_Time',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='sb_game',
            name='CAM2_id',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='sb_game',
            name='CAM3_Start_Time',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='sb_game',
            name='CAM3_id',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='sb_game',
            name='CAM4_Start_Time',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='sb_game',
            name='CAM4_id',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='sb_game',
            name='CAM5_Start_Time',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='sb_game',
            name='CAM5_id',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='sb_game',
            name='CAM6_Start_Time',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='sb_game',
            name='CAM6_id',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='sb_game',
            name='CAM7_Start_Time',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='sb_game',
            name='CAM7_id',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='sb_game',
            name='CAM8_Start_Time',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='sb_game',
            name='CAM8_id',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='sb_game',
            name='camera_cnt',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
