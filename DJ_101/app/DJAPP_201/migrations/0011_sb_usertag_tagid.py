# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-10-21 17:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('DJAPP_201', '0010_sb_usertag_clip_notes'),
    ]

    operations = [
        migrations.AddField(
            model_name='sb_usertag',
            name='tagid',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
