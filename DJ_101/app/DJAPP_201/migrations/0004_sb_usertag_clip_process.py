# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-05-19 02:33
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('DJAPP_201', '0003_auto_20170518_1948'),
    ]

    operations = [
        migrations.AddField(
            model_name='sb_usertag',
            name='clip_process',
            field=models.NullBooleanField(default=False),
        ),
    ]
