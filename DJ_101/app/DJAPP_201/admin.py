from django.contrib import admin

# Register your models here.
from .models import SB_Owner, SB_UserTag, SB_Game

admin.site.register(SB_Owner)
admin.site.register(SB_UserTag)
admin.site.register(SB_Game)