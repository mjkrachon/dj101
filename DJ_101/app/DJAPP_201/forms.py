﻿from django import forms
from datetime import datetime, timedelta
from app.DJAPP_201.models import SB_UserTag, SB_Game, SB_Owner, SB_RaspberryPiData

class SB_UserTagForm2(forms.ModelForm):

    class Meta:
        model = SB_UserTag
        fields = ('clip_rating','clip_process','clip_share','clip_notes','tagid', )

class SB_GameSelect(forms.ModelForm):

    class Meta:
        model = SB_UserTag
        fields = ('gamenum', )


class SB_UserTagForm(forms.ModelForm):
    tagrequest_ts = forms.DateTimeField(widget=forms.HiddenInput(), initial=datetime.now)
    clip_rating = forms.IntegerField(widget=forms.HiddenInput(),help_text="Clip Rating (0-10)", required=False)
    clip_share = forms.NullBooleanField(widget=forms.HiddenInput(),help_text="Share Clip?", required=False )
    clip_notes = forms.CharField(widget=forms.HiddenInput(),max_length=256, help_text="Add Notes", required=False)

    class Meta:
        model = SB_UserTag
        fields = ('clip_rating',)


class SB_GameForm(forms.ModelForm):
   # id = forms.IntegerField(widget=forms.HiddenInput)
    home_team = forms.CharField(max_length=30, help_text="Enter Home Team")
    away_team = forms.CharField(max_length=30, help_text="Enter Away Team")
    gamenum = forms.IntegerField(widget=forms.HiddenInput, initial=1000)
    gamenumstr = forms.CharField(widget=forms.HiddenInput(),max_length=30, initial=100)
    #game_timestamp = forms.DateTimeField(widget=forms.HiddenInput(), initial=datetime.now)
    startrequest_timestamp = forms.DateTimeField(widget=forms.HiddenInput(), initial=datetime.now)
    in_progress = forms.BooleanField(widget=forms.HiddenInput(), initial=True)
    game_length = forms.IntegerField(max_value = 60, help_text="Enter Game Length (minutes) 1 to 60")

    class Meta:
        model = SB_Game
        fields = ('home_team', 'away_team', 'gamenum','gamenumstr','in_progress', 'game_length')

class SB_OwnerForm(forms.ModelForm):
    first_name = forms.CharField(max_length=30, help_text="Enter First Name")
    last_name = forms.CharField(max_length=30, help_text="Enter Last Name")
    email = forms.EmailField(help_text="Enter Email Address")
    password = forms.CharField(max_length=30, help_text="Enter Password")
    ownerid = forms.IntegerField()

    class Meta:
        model = SB_Owner
        fields = ('first_name', 'last_name', 'email', 'password')

class SB_RaspberryPiDataForm(forms.ModelForm):
    SBunit_id = forms.IntegerField()
    camera_on = forms.NullBooleanField()

    class Meta:
        model = SB_RaspberryPiData
        fields = ('camera_on', 'SBunit_id')