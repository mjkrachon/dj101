"""
Definition of views.
"""
import datetime
import django
from django.utils import timezone

from django.shortcuts import render, redirect
from django.http import HttpRequest
from django.template import RequestContext
from datetime import datetime, timedelta
from django.utils import timezone
from django.http import HttpResponse
from app.DJAPP_201.forms import SB_UserTagForm, SB_UserTagForm2, SB_GameSelect, SB_OwnerForm, SB_GameForm, SB_RaspberryPiDataForm
from django.shortcuts import render_to_response
from django import forms
from app.DJAPP_201.models import SB_Owner, SB_Game, SB_UserTag, SB_RaspberryPiData
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate, login
from app.forms import BootstrapAuthenticationForm
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

def home(request):
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/index.html',
        {
            'title':'Home Page',
            'year':datetime.now().year,
        }
    )

def contact(request):
    """Renders the contact page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/contact.html',
        {
            'title':'Contact',
            'message':'SkillsBoss is brought to you by the minds of Matt Krachon.',
            'year':datetime.now().year,
        }
    )

def about(request):
    """Renders the about page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/about.html',
        {
            'title':'About',
            'message':'SkillsBoss is intended for entertainment and educational purporses to allow parents and skills trainers to generate and catalog real time video clips of youth sport action.',
            'year':datetime.now().year,
        }
    )

def user(request):
    """Renders the user page"""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/about.html',
        {
            'title':"Welcome to the Skillsboss interface page for users",
            'message':"here is a message",
            'year':datetime.now(),
        }
    )
def search_form(request):
    return render(request, 'search_form.html')

def search(request):
    if 'q' in request.GET:
        message = 'You searched for: %r within gamenumber:%r' % (request.GET['q'], request.GET['r'])
    else:
        message = 'You submitted an empty form.'
    return HttpResponse(message)

def add_SB_UserTag(request):
    #context = RequestContext(request)
    
    if request.method == 'POST':
        form = SB_UserTagForm(request.POST)
        
        if form.is_valid():
            form.save(commit=True)

            #Load in progress Game Instance
            game = SB_Game.objects.get(in_progress=True) 
            
            #Find current user ID
            if request.user.is_authenticated():
                user = request.user.id
            
            #create database instance for usertag
            tag = SB_UserTag.objects.latest('id')
            tag.ownerid = user
            tag.gamenum = game.gamenum
            tag.recordtime_ts = (tag.tagrequest_ts - game.startrequest_timestamp).total_seconds() #change to game_timestamp
            tag.recordtime_min = int(tag.recordtime_ts/60)
            tag.recordtime_sec = tag.recordtime_ts - (tag.recordtime_min * 60)
            tag.tagid = tag.id
            tag.save()

            if SB_Game.objects.filter(in_progress="True").count() > 0:
                #Find current user ID
                if request.user.is_authenticated():
                    user = request.user.id

                #load in progress game instance
                game = SB_Game.objects.get(in_progress="True")
                SB_Game_list = SB_Game.objects.filter(in_progress=True)
                SB_UserTag_list = SB_UserTag.objects.filter(ownerid=user,gamenum=game.gamenum)
                SB_Owner_list = SB_Owner.objects.filter(ownerid=user)
#               SB_RaspberryPiData = SB_RaspberryPiData.objects.filter(SBunit_id=1)
                
                paginator = Paginator(SB_UserTag_list, 2) # Show 1 contacts per page
                page = request.GET.get('page')
            
                try:
                    usertaglist = paginator.page(page)
                except PageNotAnInteger:
                    # If page is not an integer, deliver first page.
                    usertaglist = paginator.page(1)
                except EmptyPage:
                    # If page is out of range (e.g. 9999), deliver last page of results.
                    usertaglist = paginator.page(paginator.num_pages)
       
                return render(request, 'add_SB_UserTag.html', {'form': form,'SB_Game': SB_Game_list, 'usertaglist': usertaglist, 'SB_Owner':SB_Owner_list})
            
            else:
                return add_SB_Game(request)
           
        else:
            print(form.Meta.model.unique_error_message)

    else:

        form = SB_UserTagForm()

        #check if existing game has started
        if SB_Game.objects.filter(in_progress="True").count() > 0:
            #Find current user ID
            if request.user.is_authenticated():
                user = request.user.id

            #load in progress game instance
            game = SB_Game.objects.get(in_progress="True")
            SB_Game_list = SB_Game.objects.filter(in_progress=True)
            SB_UserTag_list = SB_UserTag.objects.filter(ownerid=user,gamenum=game.gamenum)
            SB_Owner_list = SB_Owner.objects.filter(ownerid=user)

            paginator = Paginator(SB_UserTag_list, 2) # Show 25 contacts per page
            page = request.GET.get('page')
            
            try:
                usertaglist = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                usertaglist = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                usertaglist = paginator.page(paginator.num_pages)
       
            #return render(request, 'view_SB_UserTag.html', {'form': form,'usertaglist': usertaglist, 'SB_Owner':SB_Owner_list})
            return render(request, 'add_SB_UserTag.html', {'form': form,'SB_Game': SB_Game_list, 'usertaglist': usertaglist, 'SB_Owner':SB_Owner_list})
        else:
            return add_SB_Game(request)

def add_SB_UserTag_Button(request):
    #check if existing game has started
    if SB_Game.objects.filter(in_progress="True").count() > 0:
        #Load in progress Game Instance
        game = SB_Game.objects.get(in_progress=True) 
        print ("Game In Progress Identified")
            
        user = request.GET.get('userid')
        print (user)

        #Cross reference user with SB_Owner table to retrieve email
        userid = SB_Owner.objects.get(ownerid=user)
        print (userid.email)

        #############create database instance for usertag
        #Load latest tag
        tag = SB_UserTag.objects.latest('id')
        tagid = tag.id
        button_tag = SB_UserTag.objects.create (id = int(tagid) + 1)
        
        #Load new Tag
        tag = SB_UserTag.objects.latest('id')
        tag.ownerid = user
        tag.gamenum = game.gamenum
        print(tag.gamenum)

####################Time Zone Information set to UTC may require update######################
        print (tag.tagrequest_ts)
        print (datetime.now())
        tag.tagrequest_ts = timezone.now()
        print (tag.tagrequest_ts)
        print (game.startrequest_timestamp)
####################
        
        tag.recordtime_ts = (tag.tagrequest_ts - game.startrequest_timestamp).total_seconds() #change to game_timestamp
        tag.recordtime_min = int(tag.recordtime_ts/60)
        tag.recordtime_sec = tag.recordtime_ts - (tag.recordtime_min * 60)
        tag.tagid = tag.id
        print (tag.tagid)
        print (tag.recordtime_ts)
        tag.save()
    else:
        print ("Game not Found")

    return redirect('home')


def view_SB_UserTag(request):
    
    if request.method == 'POST':

        form = SB_GameSelect(request.POST)
        if form.is_valid():
            gamenum = form.cleaned_data['gamenum']
            if gamenum == 0:
                gamenumfilter = "No"
            else:
                gamenum = int(gamenum)
                gamenumfilter = "Yes"
                print (gamenum)

        form = SB_UserTagForm2(request.POST)
        if form.is_valid():
            tagid = form.cleaned_data['tagid']
            tag = SB_UserTag.objects.get(id=tagid)
            tag.clip_notes = form.cleaned_data['clip_notes']
            tag.clip_share = form.cleaned_data['clip_share']
            if form.cleaned_data['clip_process']=='1':
                tag.clip_process = 1
            else:
                tag.clip_process = 0
            tag.clip_rating = form.cleaned_data['clip_rating']
            tag.save()

        #check if user is authenticated
        if request.user.is_authenticated():
            user = request.user.id
            print (user)
            #load tags for user
            if gamenumfilter == "Yes":
                SB_UserTag_list = SB_UserTag.objects.filter(ownerid=user,gamenum=gamenum).order_by('-clip_rating','-id')
            else:
                SB_UserTag_list = SB_UserTag.objects.filter(ownerid=user).order_by('-clip_rating','-id')           
                
            SB_Owner_list = SB_Owner.objects.filter(ownerid=user)
    
            #Seperate results into multiple pages to improve site responsiveness    
            paginator = Paginator(SB_UserTag_list, 1) # Show 25 contacts per page
            page = request.GET.get('page')

            try:
                usertaglist = paginator.page(page)
            except PageNotAnInteger:
            
            # If page is not an integer, deliver first page.
                usertaglist = paginator.page(1)
            except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
                usertaglist = paginator.page(paginator.num_pages)
       
            return render(request, 'view_SB_UserTag.html', {'form': form,'usertaglist': usertaglist, 'SB_Owner':SB_Owner_list})           
            
        else:
            print(form.Meta.model.unique_error_message)
    
    else:    
        form = SB_UserTagForm2()

        #check if user is authenticated
        if request.user.is_authenticated():
            user = request.user.id
        print (user)
        #load tags for user
        SB_UserTag_list = SB_UserTag.objects.filter(ownerid=user).order_by('-clip_rating','-id')
        SB_Owner_list = SB_Owner.objects.filter(ownerid=user)
    
        #Seperate results into multiple pages to improve site responsiveness    
        paginator = Paginator(SB_UserTag_list, 1) # Show 25 contacts per page

        page = request.GET.get('page')
        try:
            usertaglist = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            usertaglist = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            usertaglist = paginator.page(paginator.num_pages)
       
        return render(request, 'view_SB_UserTag.html', {'form': form,'usertaglist': usertaglist, 'SB_Owner':SB_Owner_list})

def add_SB_Game(request):
    
    #check if existing game has started
    if SB_Game.objects.filter(in_progress="True").count() > 0:
        SB_Game_list = SB_Game.objects.filter(in_progress=True)
        context_dict = {'SB_Game': SB_Game_list}     
        return render_to_response('index_game.html', context_dict)
    
    #if no games have started start form to create new game
    else:        
        if request.method == 'POST':
            form = SB_GameForm(request.POST)
            if form.is_valid():
                form.save(commit=True)

                #set game number to be game id plus 1000
                game = SB_Game.objects.latest('id')
                game.gamenum = game.id + 1000
                #game.startrequest_timestamp = datetime.now()
                game.startrequest_timestamp = timezone.now()
                print (game.startrequest_timestamp)
                game.save()

                return index_game(request)

            else:
                print(form.Meta.model.unique_error_message)

        else:
            form = SB_GameForm()

    return render(request, 'add_SB_Game.html', {'form': form})

def stop_SB_Game(request):

    context = RequestContext(request)
    #Stop all games in progress
    game = SB_Game.objects.get(in_progress=True)
    game.in_progress = False
    game.save() 
    form = SB_GameForm()
    return render(request, 'add_SB_Game.html', {'form': form})

def add_SB_Owner(request): #Module working to add user to SB_Owners list and Auth_User django database
    context = RequestContext(request)
        
    if request.method == 'POST':
        form = SB_OwnerForm(request.POST)
        
        #add additional user fields
        if form.is_valid():
            # add user if not already existing, and log-in
            username = request.POST.get('email', None)
            password = request.POST.get('password', None)
            first_name = request.POST.get('first_name', None)
            last_name = request.POST.get('last_name', None)
            user, created = User.objects.get_or_create(username=username, email=username)

            if created:
                user.set_password(password)
                user.first_name = first_name
                user.last_name = last_name
                user.save()
                form.save(commit=True)

            userlogin = authenticate(username=username, password=password)
            login(request, userlogin)
            if request.user.is_authenticated():
                #If new user is created transfer user id to database
                user = SB_Owner.objects.latest('id')
                user.ownerid = request.user.id
                user.save()
                #form.save(commit=True)
            return home(request)
        else:
            print(form.Meta.model.unique_error_message)

    else:
        form = SB_OwnerForm()

    return render(request, 'add_SB_Owner.html', {'form': form})

def index(request):
    context = RequestContext(request)
    
    SB_Owner_list = SB_Owner.objects.order_by('-id')[:20]
    context_dict = {'SB_Owner': SB_Owner_list}
        
    return render_to_response('index2.html', context_dict, context)

def index_game(request):
    context = RequestContext(request)
    SB_Game_list = SB_Game.objects.order_by('-id')[:50]
    context_dict = {'SB_Game': SB_Game_list}
        
    return render_to_response('index_game.html', context_dict, context)

def index_usertag(request):
    context = RequestContext(request)
    SB_Game_list = SB_Game.objects.order_by('-id')[:50]
    context_dict = {'SB_Game': SB_Game_list}
        
    return render_to_response('index_game.html', context_dict, context)