"""
Definition of urls for DJ_101.
"""

from datetime import datetime
from app.views import add_SB_UserTag_Button
from django.conf.urls import url
import django.contrib.auth.views
from DJ_101.views import hello

# from django.conf.urls import path

import app.forms
import app.views
# from app.DJAPP_201 import views **This code should work in place of the following line
from app.views import search_form, search
# import DJ_101

# Uncomment the next lines to enable the admin:
from django.conf.urls import include
from django.contrib import admin
admin.autodiscover()

urlpatterns = [
    # Examples:
    url(r'^$', app.views.home, name='home'),
    url(r'^home$',app.views.home, name='home'),
    url(r'^contact$', app.views.contact, name='contact'),
    url(r'^about', app.views.about, name='about'),
    #login,
    url(r'^login/$',
        django.contrib.auth.views.login,
        {
            'template_name': 'app/login.html',
            'authentication_form': app.forms.BootstrapAuthenticationForm,
            'extra_context':
            {
                'title': 'Log in',
                'year': datetime.now().year,
            }
        },
        name='login'),
    #logout
    url(r'^logout$',
        django.contrib.auth.views.logout,
        {
            'next_page': '/',
        },
        name='logout'),
    url(r'^hello$',hello),
    url(r'^user$',app.views.user),
    url(r'^search-form/$', search_form),
    url(r'^search/$', search),
    url(r'^add_SB_UserTag/$', app.views.add_SB_UserTag, name='Add_SB_UserTag'),
    #url(r'^add_SB_UserTag_Button/(?P<userid>[0-9]+)$', app.views.add_SB_UserTag_Button, name='add_SB_UserTag_Button'),
    url(r'^add_SB_UserTag_Button/$', app.views.add_SB_UserTag_Button, name='add_SB_UserTag_Button'),
    url(r'^view_SB_UserTag/$', app.views.view_SB_UserTag, name='View_SB_UserTag'),
    url(r'^add_SB_Game/$', app.views.add_SB_Game, name='Add_Game'),
    url(r'^stop_SB_Game/$', app.views.stop_SB_Game, name='Stop_SB_Games'),
    url(r'^add_SB_Owner/$', app.views.add_SB_Owner, name='add_SB_Owner'),

    # Uncomment the admin/doc line below to enable admin documentation:
    #url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
]
